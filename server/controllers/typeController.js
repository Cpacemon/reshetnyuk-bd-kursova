const {Type} = require('../models/models')
const ApiError = require('../error/ApiError');
const Pool = require('pg').Pool
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    password: 'root',
    port: 5432,
})


class TypeController {
    async create(req, res) {
        const {name} = req.body
        const type = await Type.create({name})
        return res.json(type)
    }

    async getAll(req, res) {
        const types = await Type.findAll()
        return res.json(types)
    }

    async delete(req, res) {
        const {id} = req.body
        await Type.destroy({where: {id}})
        return res.json('Sucsess')

    }

   async updateType (req, res)  {
        const id = req.params.id;
        Type.update(req.body, {
            where: { id: id }
        })
            .then(num => {
                if (num == 1) {
                    res.send({
                        message: "Type was updated successfully."
                    });
                } else {
                    res.send({
                        message: `Cannot update Type with id=${id}. Maybe Type was not found or req.body is empty!`
                    });
                }
            })
            .catch(err => {
                res.status(500).send({
                    message: "Error updating Type with id=" + id
                });
            });
    };

}
    module.exports = new TypeController()

