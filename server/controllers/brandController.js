const {Brand, Type} = require('../models/models')
const ApiError = require('../error/ApiError');

class BrandController {
    async create(req, res) {
        const {name} = req.body
        const brand = await Brand.create({name})
        return res.json(brand)
    }

    async getAll(req, res) {
        const brands = await Brand.findAll()
        return res.json(brands)
    }

    async delete(req,res){
        const {id} = req.body
        await Brand.destroy({where:{id}})
        return res.json('Sucsess')

    }

    async updateBrand (req, res)  {
        const id = req.params.id;
        Type.update(req.body, {
            where: { id: id }
        })
            .then(num => {
                if (num == 1) {
                    res.send({
                        message: "Brand was updated successfully."
                    });
                } else {
                    res.send({
                        message: `Cannot update Brand with id=${id}. Maybe Brand was not found or req.body is empty!`
                    });
                }
            })
            .catch(err => {
                res.status(500).send({
                    message: "Error updating Brand with id=" + id
                });
            });
    };

}

module.exports = new BrandController()
