const Router = require('express')
const router = new Router()
const deviceController = require('../controllers/deviceController')
const typeController = require("../controllers/typeController");

router.post('/', deviceController.create)
router.get('/', deviceController.getAll)
router.get('/:id', deviceController.getOne)
router.delete('/',deviceController.delete)

module.exports = router
